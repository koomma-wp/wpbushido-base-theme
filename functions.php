<?php
/**
 * wptm functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wptm
 *
 * ! PLEASE USE INCLUDE/REQUIRE FOR YOUR SPECIFIC PROJECT CODE
 */


if (!function_exists('is_plugin_active')) {
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

/**
 * Theme
 */

/*----------  Theme setup  ----------*/
require_once get_template_directory() . '/functions/setup.theme.php';

/*----------  Theme init  ----------*/
require_once get_template_directory() . '/functions/init.theme.php';

/*----------  Assets management  ----------*/
require_once get_template_directory() . '/functions/assets.theme.php';

/**
 * Admin
 */

require_once get_template_directory() . '/functions/custom.admin.php';


/**
 * Core
 */

/*----------  Sessions  ----------*/
require_once get_template_directory() . '/functions/sessions.core.php';


/**
 * Plugins
 */
/*----------  ACF management  ----------*/
require_once get_template_directory() . '/functions/acf.plugin.php';

/*----------  Memcache support  ----------*/
require_once get_template_directory() . '/functions/memcache.plugin.php';
