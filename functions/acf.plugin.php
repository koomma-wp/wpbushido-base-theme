<?php

if (is_admin() && is_plugin_active('advanced-custom-fields-pro/acf.php')) {
    /*----------  Auto export ACF changes as JSON  ----------*/
    function wptm_acf_json_save_point( $path ) {
        // update path
        $path = get_template_directory() . '/acf-json';
        // return
        return $path;
    }
    add_filter('acf/settings/save_json', 'wptm_acf_json_save_point');
}