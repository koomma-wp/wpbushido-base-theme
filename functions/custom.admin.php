<?php

if (!defined('WPTM_ADMIN_CUSTOM_EDITOR_STYLE')) {
    define('WPTM_ADMIN_CUSTOM_EDITOR_STYLE', 'custom-editor-style.css');
}
add_editor_style(WPTM_ADMIN_CUSTOM_EDITOR_STYLE);

if (defined('WPTM_ADMIN_BAR_BACKGROUND')) {
    function wptm_change_bar_color()
    {

        $colorBgAdmin = WPTM_ADMIN_BAR_BACKGROUND;
        ?>
        <style>
        #wpadminbar{
            background: <?php echo $colorBgAdmin; ?> !important;
        }
        </style>
        <?php
    }
    add_action('wp_head', 'wptm_change_bar_color');
    add_action('admin_head', 'wptm_change_bar_color');
}
