<?php

// Client Setting
if (!is_admin() || (defined('DOING_AJAX') && DOING_AJAX)) {
    if (class_exists('WPBushidoProject\Client\Client')) {
        $timber = new \Timber\Timber();
        $clientWp = new WPBushidoProject\Client\Client();
    }
}

/*----------   Remove inline Recent Comment Styles from wp_head()  ----------*/
function wptm_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}
add_action('widgets_init', 'wptm_remove_recent_comments_style');

/*----------  Remove emojis  ----------*/
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );