<?php

if (is_plugin_active('kitae-memcached/kitae-memcached.php')) {
    /**
     * Get cached elements using Memcache
     */
    function wptm_memcached_data() {
        if (defined('WPTM_MEMCACHE') && WPTM_MEMCACHE === true && defined('WPTM_MEMCACHE_HOST')) {
            kitae_memcached_add_server(WPTM_MEMCACHE_HOST, 11211);
        } else {
            return false;
        }

        /*
            ## Sample memcache use
            if (function_exists('kitae_memcached_get') && kitae_memcached_instance()->getEnabled() && ($cached_content = kitae_memcached_get('CACHED_OBJECT_NAME'))) {
                $result = $cached_content;
            } else if (function_exists('kitae_memcached_set') && kitae_memcached_instance()->getEnabled()) {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL            => 'http://localhost/content/to/get',
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_TIMEOUT        => 10
                ));

                $result = curl_exec($curl);
                curl_close($curl);
                $result = json_decode($result, true);
                if (!empty($result)) {
                    kitae_memcached_set('CACHED_OBJECT_NAME', $result);
                }
            }
         */
    }
    add_action('init', 'wptm_memcached_data');
}