<?php

function wptm_startSession()
{
    if(!session_id()) {
        session_start();
    }
}

function wptm_endSession()
{
    if(session_id()) {
        session_destroy();
    }
}

add_action('init', 'wptm_startSession', 1);
add_action('wp_logout', 'wptm_endSession');
add_action('wp_login', 'wptm_endSession');