<?php

if (!function_exists('wptm_setup')) {
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function wptm_setup() {
        if (defined('WPTM_LANGUAGE_DOMAIN')) {
            load_theme_textdomain( WPTM_LANGUAGE_DOMAIN, get_template_directory() . '/languages' );
        }

        // Add Menu Support
        add_theme_support('menus');

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );
    }
}
add_action( 'after_setup_theme', 'wptm_setup' );